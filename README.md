# **YBC tables**

This repository contains the tables computed with YBC code. There are three categories and they are distributed in three folders:
1. YBC: the normal BC tables
2. rYBC: the tables BC for rotating stars
3. iYBC: the limb-darkening coefficients with Kurucz libraries.
If you are not familiar with `.fits` files, you can use `topcat` to convert them to ascii files (`topcat`: http://www.star.bris.ac.uk/~mbt/topcat/), or ask me to do so (cy@ahu.edu.cn).

## Nomenclature
The tables within `FILTER/regrid/` contains:

1. Phoenix tables produced with spectra by Prof. Allard France (no more available, https://phoenix.ens-lyon.fr/Grids/BT-Settl/AGSS2009/SPECTRA/, or https://www.stsci.edu/hst/instrumentation/reference-data-for-calibration-and-tools/astronomical-catalogs/phoenix-models-available-in-synphot)
```
Avodonnell94Rv3.1BT-Settl_M-0.0_a+0.0.BC.fits
Avodonnell94Rv3.1BT-Settl_M+0.3_a+0.0.BC.fits
Avodonnell94Rv3.1BT-Settl_M+0.5_a+0.0.BC.fits
Avodonnell94Rv3.1BT-Settl_M-0.5_a+0.2.BC.fits
Avodonnell94Rv3.1BT-Settl_M-1.0_a+0.4.BC.fits
Avodonnell94Rv3.1BT-Settl_M-1.5_a+0.4.BC.fits
Avodonnell94Rv3.1BT-Settl_M-2.0_a+0.4.BC.fits
Avodonnell94Rv3.1BT-Settl_M-2.5_a+0.4.BC.fits
Avodonnell94Rv3.1BT-Settl_M-3.0_a+0.4.BC.fits
Avodonnell94Rv3.1BT-Settl_M-3.5_a+0.4.BC.fits
Avodonnell94Rv3.1BT-Settl_M-4.0_a+0.4.BC.fits
```
`Avodonnell94Rv3.1` means with odonnell94 extinction curve (with Rv=3.1).
`BT-Settl_M-0.0_a+0.0.` means with the set `BT-Settl` of [M/H]=-0.0 and [a/Fe]=0.0.

2. CK2004 atlas9 models (https://wwwuser.oats.inaf.it/castelli/grids.html)
```
Avodonnell94Rv3.1fm05k2odfnew.BC.fits
Avodonnell94Rv3.1fm10k2odfnew.BC.fits
Avodonnell94Rv3.1fm15k2odfnew.BC.fits
Avodonnell94Rv3.1fm20k2odfnew.BC.fits
Avodonnell94Rv3.1fm25k2odfnew.BC.fits
Avodonnell94Rv3.1fp00k2odfnew.BC.fits
Avodonnell94Rv3.1fp02k2odfnew.BC.fits
Avodonnell94Rv3.1fp05k2odfnew.BC.fits
```
`fm05k2odfnew`: `m05` means [M/H]=-0.05 (`p05` means [M/H]=0.05).

3. ATLAS12 models for the two populations of 47 Tuc (computed by us, https://gitlab.com/cycyustc/spec_ybc)
```
Avodonnell94Rv3.147TucP1.BC.fits
Avodonnell94Rv3.147TucP2.BC.fits
```

4. COMARCS models for Carbon stars and M stars (computed by Dr. Aringer of us, http://stev.oapd.inaf.it/synphot/Cstars/)
```
Avodonnell94Rv3.1COMARCS_Cstars_o83029_c83241_fe70630.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_o83029_c83443_fe70630.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_o88029_c88073_fe75630.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_o88029_c88241_fe75630.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_o88029_c88443_fe75630.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_o88029_c89491_fe75630.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_o88029_c91040_fe75630.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_z0010_czuo0140_xi25.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_z0010_czuo0200_xi25.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_z0010_czuo0500_xi25.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_z0033_czuo0140_xi25.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_z0033_czuo0200_xi25.BC.fits
Avodonnell94Rv3.1COMARCS_Cstars_z0033_czuo0500_xi25.BC.fits
Avodonnell94Rv3.1COMARCS_Mstars_mstar_feh+0.00_bt2.dat.BC.fits
Avodonnell94Rv3.1COMARCS_Mstars_mstar_feh+0.50_bt2.dat.BC.fits
Avodonnell94Rv3.1COMARCS_Mstars_mstar_feh-0.50_bt2.dat.BC.fits
Avodonnell94Rv3.1COMARCS_Mstars_mstar_feh+1.00_bt2.dat.BC.fits
Avodonnell94Rv3.1COMARCS_Mstars_mstar_feh-1.00_bt2.dat.BC.fits
Avodonnell94Rv3.1COMARCS_Mstars_mstar_feh-1.50_bt2.dat.BC.fits
Avodonnell94Rv3.1COMARCS_Mstars_mstar_feh-2.00_bt2.dat.BC.fits
```

5. PoWR models for WR stars (https://www.astro.physik.uni-potsdam.de/~wrh/PoWR/powrgrid1.php)
```
Avodonnell94Rv3.1lmc-wcgrid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1lmc-wnegrid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1lmc-wnl20grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1lmc-wnl40grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1smc-wnegrid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1smc-wnl20grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1smc-wnl40grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1smc-wnl60grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1sol-wcgrid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1sol-wnegrid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1sol-wnl20grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1sol-wnl50grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1subsmc-wnegrid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1subsmc-wnl20grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1subsmc-wnl40grid.200-80000.extended.ori.BC.fits
Avodonnell94Rv3.1subsmc-wnl60grid.200-80000.extended.ori.BC.fits
```

6. WMbasic models (computed by us, https://gitlab.com/cycyustc/spec_ybc)
```
Avodonnell94Rv3.1WM_Z0.0001Mdot5.BC.fits
Avodonnell94Rv3.1WM_Z0.0001Mdot6.BC.fits
Avodonnell94Rv3.1WM_Z0.0001Mdot7.BC.fits
Avodonnell94Rv3.1WM_Z0.0005Mdot5.BC.fits
Avodonnell94Rv3.1WM_Z0.0005Mdot6.BC.fits
Avodonnell94Rv3.1WM_Z0.0005Mdot7.BC.fits
Avodonnell94Rv3.1WM_Z0.001Mdot5.BC.fits
Avodonnell94Rv3.1WM_Z0.001Mdot6.BC.fits
Avodonnell94Rv3.1WM_Z0.001Mdot7.BC.fits
Avodonnell94Rv3.1WM_Z0.004Mdot5.BC.fits
Avodonnell94Rv3.1WM_Z0.004Mdot6.BC.fits
Avodonnell94Rv3.1WM_Z0.004Mdot7.BC.fits
Avodonnell94Rv3.1WM_Z0.008Mdot5.BC.fits
Avodonnell94Rv3.1WM_Z0.008Mdot6.BC.fits
Avodonnell94Rv3.1WM_Z0.008Mdot7.BC.fits
Avodonnell94Rv3.1WM_Z0.02Mdot5.BC.fits
Avodonnell94Rv3.1WM_Z0.02Mdot6.BC.fits
Avodonnell94Rv3.1WM_Z0.02Mdot7.BC.fits
```

7. TLUSTY models for hot stars of different H/He combinations (eg. hot dW) (computed by us, seems not uploaded, to do soon)
```
Avodonnell94Rv3.1Tlusty_H1.0.BC.fits
Avodonnell94Rv3.1Tlusty_H0.9.BC.fits
Avodonnell94Rv3.1Tlusty_H0.8.BC.fits
Avodonnell94Rv3.1Tlusty_H0.7.BC.fits
Avodonnell94Rv3.1Tlusty_H0.6.BC.fits
Avodonnell94Rv3.1Tlusty_H0.5.BC.fits
Avodonnell94Rv3.1Tlusty_H0.4.BC.fits
Avodonnell94Rv3.1Tlusty_H0.3.BC.fits
Avodonnell94Rv3.1Tlusty_H0.2.BC.fits
Avodonnell94Rv3.1Tlusty_H0.1.BC.fits
Avodonnell94Rv3.1Tlusty_H0.0.BC.fits
```

8. Koester for white dwarfs (http://svo2.cab.inta-csic.es/theory/newov2/index.php?models=koester2)
```
Avodonnell94Rv3.1Koester.BC.fits
```

## table column description
1. logTeff: Teff in logarithm
2. logg: log of gravity in cgs
3-Nth: BCs for each filter with different Av values. Those without `_Av???` are with Av=0.


## Installing/downloading
You can simply download the whole tables directly with:
```
wget -c http://sec.center/YBC/downloads/ybc_tables_20211014.tar.gz
```
and ignore the rest of this section. However, the connection might be not very stable and be very slow, in which case you should try the following steps.

0. global setting, just do it once on your computer:
```
git lfs install
git config --global filter.lfs.smudge "git-lfs smudge --skip"
git config --global filter.lfs.process "git-lfs filter-process --skip"
```

1. just for new installation:
```
mkdir YBC_tables
cd YBC_tables
git init
git remote add origin https://gitlab.com/cycyustc/YBC_tables.git
(or git remote add origin git@gitlab.com:cycyustc/YBC_tables.git)
git pull origin master
```

2. to update your local copy:
```
cd YBC_tables
git pull origin master
```

3. to download the tables for a specific filter set:
```
cd YBC_tables
python install.py FILTER YBC/rYBC/iYBC
```
`FILTER` is the filter set name as listed in `filters.list`.
Use `\*` or `"*"`, if you would like to download tables for all the filter sets. However, it takes some time (10 mins).
And you choose one of `YBC`, `rYBC` and `iYBC` to download.


## File description
```
1. `extinctionodonnell94Rv3.1.al` (or `.al` file): extinction vector `Al` for each of the filter
2. `filter.info`: infomation for the filters. The columns are filter number, filter name, filter file used, effective wavelength (in Angstrom), band width, absolute flux of Vega (in unit of erg cm-2 s-1 A-1), photometric system (AB, Vega, ST or TG), type of device (energy counting or number counting for CCD), reference magnitude of Vega applied, irrelevant value, comments.
3. `*.fits`: the BC tables, either in `FILTER/regrid` or `FILTER/ori`.
```


## Fits header keywords
```
`MBOL_SUN`: bolometric magnitude of the Sun as the IAU2015 resolution
`LUM_SUN`: solar luminosity as the IAU2015 resolution
`VG_REF`: Vega calibration used
`VG_FILTERi`: Vega reference magnitude in a filter. If it is `NULL` it may mean it is really zero or just Unset. For some filters, the keyword is longer than 9, therefore, "HIERARCH VG_FILTERi" has been automatically used by FITSIO standard.
`VERSION`: date of the generation/modification of the tables
```


## Reference
Chen+2019: [https://ui.adsabs.harvard.edu/abs/2019A%26A...632A.105C/abstract]


## Usage for TRILEGAL users
If you have TRILEGAL installed, later you can make a symbolic link `$TRILEGALDIR/YBC` to the `YBC_tables` folder.
Then follow the instruction in `README_YBC.md` from trilegal.

## Previous versions:
```
wget -c http://sec.center/YBC/downloads/ybc_tables_20211014.tar.gz (current vesion)
http://sec.center/YBC/downloads/YBC_tables.20210422.tar.gz
http://sec.center/YBC/downloads/YBC_tables.20210413.tar.gz
http://sec.center/YBC/downloads/YBC_tables.before_20210412.tar.gz
http://sec.center/YBC/downloads/YBC_tables.before_20210101.tar.gz
```
Notice the dates only indicate the approximate time of creating these compressed files, other than the time the tables being updated.

## Update history
2021-04-22: 1. start to put modification date in the fits header. 2. make denser MIR wavelength grids of the Castelli & Kurucz (2004) spectra, to have better MIR BCs.
For other logs please check the `log.txt` file.



## Caveats
1. old COMARCS carbon star spectra are causing problems in the blue part (~< 4000 A). So they are removed from `YBC/YBC/Avodonnell94Rv3.1YBC.list`.
2. vertical line behaviour for post-agbs in the hottest part.
